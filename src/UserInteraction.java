import java.util.Scanner;

public class UserInteraction {
    private Scanner scanner;

    public UserInteraction(Scanner scanner) {
        this.scanner = scanner;
    }

    public int getUserAnswer() {
        while (!scanner.hasNextInt()) {
            System.out.println("This isn't a number. Try again.");
            scanner.next();
        }
        return scanner.nextInt();
    }

    public Point getInputPoint() {
        System.out.println("Input x: ");
        var x = getInputData();
        System.out.println("Input y: ");
        var y = getInputData();
        return new Point(x, y);
    }

    public double getInputData() {
        while (!scanner.hasNextDouble()) {
            System.out.println("This isn't a number. Try again");
            scanner.next();
        }
        return scanner.nextDouble();
    }
}
