import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        UserInteraction userInteraction = new UserInteraction(scanner);
        System.out.println("Hello");
        var points = new PointList();
        while (true) {
            System.out.println(" Please input: 1 - if you want to add the point " +
                    " 2 - if you don't want to add any points ");
            var userAnswer = userInteraction.getUserAnswer();

            if (userAnswer == 1) {
                System.out.println(" Please input coordinates of the point.");
                points.add(userInteraction.getInputPoint());
            } else if (userAnswer == 2) {
                System.out.println("Thanks.");
                break;
            } else {
                System.out.println("You input wrong number. Please repeat.");
            }
        }

        if (points.size() == 0) {
            return;
        }

        System.out.println(" Please input coordinates of the center.");
        var center = userInteraction.getInputPoint();
        System.out.println(" Please input a radius of the circle.");
        System.out.println("Input radius: ");
        var circle = new Circle(center, userInteraction.getInputData());

        scanner.close();
        System.out.println("These points belong the circle: ");
        points.stream()
                .filter(circle::isPointInTheCircle)
                .forEach(System.out::println);
    }

}


