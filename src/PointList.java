import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class PointList {
    private List<Point> points;

    public PointList() {
        points = new ArrayList<>();
    }

    public void add(Point p) {
        points.add(p);
    }

    public Point get(int index) {
        return points.get(index);
    }

    public Stream<Point> stream() {
        return points.stream();
    }

    public int size() {
        return points.size();
    }
}
